package HermiT;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationSubject;
import org.semanticweb.owlapi.model.OWLAnonymousIndividual;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.SWRLArgument;
import org.semanticweb.owlapi.model.SWRLAtom;
import org.semanticweb.owlapi.model.SWRLPredicate;
import org.semanticweb.owlapi.model.SWRLRule;

import de.derivo.sparqldlapi.Query;
import de.derivo.sparqldlapi.QueryEngine;
import de.derivo.sparqldlapi.QueryResult;
import de.derivo.sparqldlapi.exceptions.QueryParserException;
import no.s11.owlapijsonld.JsonLdParserFactory;

public class ReasonerImpl  {

	private OWLOntologyManager manager;
	private OWLOntology o;
	private OWLOntology superOntolgy;
	private Reasoner hermit;
	//Name of the base ontology to compare in case multiple ontology's are imported
	private String ontString="http://users.atlantis.ugent.be/svrstich/demoOntologyZonderRules";
	
	
	public void createInstance(InputStream s) throws Exception{
		manager = OWLManager.createOWLOntologyManager();
		superOntolgy = manager.loadOntologyFromOntologyDocument(s);
		Set<OWLOntology> imported=superOntolgy.getImports();
		for(OWLOntology ont: imported){
			if(ont.getOntologyID().getOntologyIRI().toString().equals(ontString)){
				o=ont;
			}
		}				
	}
	
	public void destroyInstance() {
		o=null;
		manager=null;
		superOntolgy=null;
	}
	
	public void addKnowledge(String jsonLdString) throws Exception{
		System.out.println("add knowledge");
		InputStream stream=new ByteArrayInputStream(jsonLdString.getBytes());
		JsonLdParserFactory.register();
		OWLOntology add=manager.loadOntologyFromOntologyDocument(stream);
		
		Iterator<OWLAxiom> it= add.getAxioms().iterator();
		while(it.hasNext()){
			manager.addAxiom(o,(OWLAxiom) it.next());			
		}	
	}

	public String query(String queryString) throws Exception{
		SWRLRuleExecutor ex=new SWRLRuleExecutor(superOntolgy,o,manager);
		ex.executeSWRLRules();
		hermit=new Reasoner(o);
		dumpModel(new FileOutputStream(
				"HOntologie" + "-" + System.currentTimeMillis()
				+ ".owl"));
 		QueryEngine engine = QueryEngine.create(manager, hermit);
		Query query = Query.create(queryString);
		QueryResult result = engine.execute(query);
		return result.toJSON();
	}

	
	public void dumpModel(OutputStream os) throws Exception{
		manager.saveOntology(o, os);
	}
	
}
