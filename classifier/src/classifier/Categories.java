package classifier;

import java.util.List;

/**
 * Convenience data-object to represent the categories of a certain connection.
 * Mainly for visualisation purposes. It is nothing more than a named extension
 * of an ObservableList
 *
 * @author svrstich
 * @date 2015-09-10 - 14:28
 */
public class Categories  {

    /**
     * *************************************************************************
     * CONSTANTS *
     *************************************************************************
     */
    //Logic placeholders for the different types of Connections
    public static int MOBILITY_IMPAIRED_TIMED_SUITABLE_CONNECTIONS = 1;
    public static int BICYCLE_SUITABLE_CONNECTION = 2;
    public static int FROST_SUITABLE_CONNECTION = 3;
    public static int SNOW_SUITABLE_CONNECTION = 4;
    public static int ALL_CONNECTIONS = 5;
    public static int HEARING_IMPAIRMENT_SUITABLE_CONNECTION = 6;
    public static int VISUAL_IMPAIRMENT_SUITABLE_CONNECTION = 7;
    public static int GREEN_ORDERED_CONNECTION = 8;
    public static int PETROL_HEAD_CONNECTION = 9;
    public static int TRAIN_GEEK_CONNECTION = 10;

    /**
     * *************************************************************************
     * CONSTRUCTORS *
     *************************************************************************
     */
    public Categories(List list) {
    }

    public static int getNumericValueForString(String name) {
        int value = 0;

        //System.out.println(name);
        
        switch (name) {
            case "http://users.atlantis.ugent.be/svrstich/demoOntology#MobilityImpairmentTimedSuitableConnection":
                value = 1;
                break;
            case "http://users.atlantis.ugent.be/svrstich/demoOntology#BicycleSuitableConnection":
                value = 2;
                break;
            case "http://users.atlantis.ugent.be/svrstich/demoOntology#FrostSuitableConnection":
                value = 3;
                break;
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#SnowSuitableConnection" :
                value = 4;
                break;
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#DepartureTimeSuitableConnection" :
                value = 5;
                break;                
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#HearingImpairmentSuitableConnection" :
                value = 6;
                break;
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#VisualImpairmentSuitableConnection" :
                value = 7;
                break;
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#GreenOrderedConnection" :
                value = 8;
                break;
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#PetrolHeadOrderedConnection" :
                value = 9;
                break;
           case "http://users.atlantis.ugent.be/svrstich/demoOntology#TrainGeekOrderedConnection" :
                value = 10;
                break;               
        }

        return value;
    }

}
