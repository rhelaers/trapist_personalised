package classifier;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.annotations.ApplySchedule;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Link;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonObjectBuilder;
import org.ibcn.limeds.json.JsonValue;

import types.TypeResolver;

@Validated(input=false,output=false)
@Segment(id = "be.ugent.intec.ibcn.km.trapist.rail.dft.classifier.impl")
public class ClassifierImpl implements FunctionalSegment{

	

	/****************************************************************************************************
	 * LIMEDS: PRIVATE VARIABLES/CONSTANTS 																*
	 ****************************************************************************************************/

	// Enables debugging mode/output
	private boolean debug = false;
	private boolean info = true;
	// Request counter for debugging
	private static int requestCount = 0;
	// Station name for debugging purposes
	private String stationName = "Izegem";
	// Station id for debugging purposes (equals Izegem)
	private String stationID = "BE.NMBS.008892635";
	//The range for DeLijn stops to be considered
	private double range = 0.0;
	//Size of the departure list
	private static final int CONNECTION_LIST_SIZE = 10;
	//save ontology
	private boolean saveOntology=false;

	/****************************************************************************************************
	 * LIMEDS: DATAFLOW SERVICES 																		*
	 ****************************************************************************************************/
	
	// This is the LimeDS component responsible for fetching DeLijn departures
	@Link(target = "be.irail.delijn.departures")
	private FunctionalSegment deLijnDepartures;		
	// This is the LimeDS component responsible for fetching nearby DeLijn stops
	@Link(target = "be.irail.delijn.nearby-stations")
	private FunctionalSegment deLijnNearbyStops;	
	// This is the LimeDS component responsible for geocoding a textual address
	@Link(target = "GoogleGeocoder")
	private FunctionalSegment geocoder;
	// This is the LimeDS component responsible for fetching all NMBS connections for the given station.
	@Link(target = "be.irail.nmbs.connections")
	private FunctionalSegment iRailNMBSConnections;
	// This is the LimeDS component responsible for converting human readable NMBS station names into
	// structured IDs, cf. the stationName and stationID constants defined above.
	@Link(target = "be.irail.nmbs.stations")
	private FunctionalSegment iRailNMBSStations;
	// This is the LimeDS component responsible for executing the reasoning process, i.e.:
	// - it reads in the ontology
	// - it populates the A-Box with the supplied JSON-LD
	// - it answers - in our case - the type SPARQL queries for given connections.
	@Link(target = "HermiT")
	private FunctionalSegment personalisedConnectionReasoner;	
	
//	@Service(filter="(" + ServicePropertyNames.SEGMENT_ID + "=PersonalisedConnectionReasoner)")
//	private FunctionalSegment apersonalisedConnectionReasoner;

	/****************************************************************************************************
	 * LIMEDS: DATAFLOW SERVICE - invocation method implementation 										* 
	 * The goal of this services is multi-tiered: 														*
	 * 1) Fetch all available connections 																*
	 * 2) Enhance the received JSON into JSON-LD corresponding to the TraPIST Ontology Model 			*
	 ****************************************************************************************************/
	
	@InputDoc(schemaRef="classifier.model.ClassifierImpl_input_0.1.0")
	@OutputDoc(schemaRef="classifier.model.ClassifierImpl_output_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/endPoint/categorisedConnectionProcessor")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		System.out.println("Hallo Classifier");
			
		//For performance evaluation purposes
		long start = System.currentTimeMillis();
		
		String operation = input[0].getString("operation");
		if (debug) {
			System.out.println("TEST" + operation);
		}

		// Only data population is requested - without classification
		if (operation.equalsIgnoreCase("data")) {
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][REQUEST COUNT] "
						+ requestCount++);
				System.out
						.println("["
								+ System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][PARAMETERS PASSED ON FROM LIMEDS] "
								+ input[0]);
			}
			
			
			// STEP 0) - Flushing the ontology model
			this.flush();
			
			
			// STEP 0a) - GeoCoding the textuals location name
			JsonValue geocodedLocation = this.geocodeTextualLocationName(input[0].getString("name"));

			// STEP 0b)
			JsonValue outputStep0b = this.step0b(geocodedLocation);
			

			// STEP 1a)
			JsonValue outputStep1a = this.step1a(input);

			
			// STEP 1b)
			List<JsonValue> outputStep1b = this.step1b(outputStep0b);			
			
			// STEP 2a)
			List<JsonValue> outputStep2a = this.step2a(outputStep1a);
			
			
//			System.out.println(outputStep2a.get(0).toPrettyString());
			
			// STEP 2b)
			List<JsonValue> outputStep2b = this.step2b(outputStep1b);
			
			
			
			
			// STEP 2c) Current Timestamp population
			List<JsonValue> outputStep2c = new ArrayList<>();
			outputStep2c.add(this.getCurrentJSONTimeStamp());
			
			// STEP 3)
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][NMBS CONNECTION COUNT] "
						+ outputStep2a.size());
			}

			List<JsonValue> outputStep2asub = outputStep2a;
			if (outputStep2a.size() > CONNECTION_LIST_SIZE) {
				outputStep2asub = outputStep2a.subList(0, CONNECTION_LIST_SIZE);
			}
			
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][ADDING NMBS CONNECTION SET]["+ outputStep2asub.size() +"] "
						+ outputStep2asub);
			}			
			this.step3(outputStep2asub);
			
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][DE LIJN CONNECTION COUNT] "
						+ outputStep2b.size());
			}
			
			List<JsonValue> outputStep2bsub = outputStep2b;
			if (outputStep2b.size() > CONNECTION_LIST_SIZE) {
				outputStep2bsub = outputStep2b.subList(0, CONNECTION_LIST_SIZE);
			}
						
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][ADDING DE LIJN CONNECTION SET] "
						+ outputStep2bsub);
			}
			this.step3(outputStep2bsub);
			
			this.step3(outputStep2c);
			
			// STEP 4)
			JsonArray resulta = this.step4(outputStep2asub, false);
			JsonArray resultb = this.step4(outputStep2bsub, false);
			
			JsonArray result = resulta;
			result.addAll(resultb);

			if (info) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][INFO ][INITIALISATION RT] "
						+ (System.currentTimeMillis() - start) + " ms");
			}

			return Json.objectBuilder()
					.add("departures", result).build();
		}

		// Only classification for the given connection
		else if (operation.equalsIgnoreCase("classify")) {
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][Headsign] "
						+ input[0].getString("train"));
			}
			JsonObject result = this.step4("" + input[0].get("train"), true);
			if (info) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][INFO ][RT - QUERYING] "
						+ (System.currentTimeMillis() - start) + " ms");
			}
			return (result);
			
		}
					
		return null;
	}

	/****************************************************************************************************
	 * WORKFLOW: INDIVIDUAL STEP SUPPORT-METHODS *
	 ****************************************************************************************************/
	/****************************************************************************************************
	 * Step 0: Flushes the ontology A-Box * *
	 * 
	 * @param JsonNode
	 *            ... input *
	 * @return Optional<T> output *
	 ****************************************************************************************************/
	private <T extends JsonValue> void flush(JsonValue... input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 0 - FLUSH CURRENT ONTOLOGY MODEL]");
		}

		JsonObject connectionsInput = Json.objectBuilder().add("type", "flush")
				.build();

		try {
			personalisedConnectionReasoner.apply(connectionsInput);
		} catch (Exception e) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][ERROR][FLUSH FAILED] "
					+ e.getStackTrace());
			e.printStackTrace();
		}
		return;
	}

	/****************************************************************************************************
	 * Step 0a: Geocoding of station name for De Lijn* *
	 * 
	 * @param JsonNode
	 *            ... input *
	 * @return Optional<T> output *
	 ****************************************************************************************************/
	private JsonValue geocodeTextualLocationName(String location) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 0a - GEOCODE LOCATION]");
		}
		JsonValue output = null;
		try {
			JsonValue connectionsInput = Json.objectBuilder()
	                .add("path", Json.objectBuilder().add("addressLine", "Station "+location))
	                .build();
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG] ConnectionsInput: "
						+ connectionsInput);
			}
	        //Eerste argument van de apply een leeg JSON object meegeven
	        output = geocoder.apply(Json.objectBuilder().build(),
	                connectionsInput);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output;
	}

	/****************************************************************************************************
	 * Step 0b: Fetching nearby De Lijn stops for the given GEOLOCATION
	 * 
	 * @param JsonNode
	 *            ... input *
	 * @return Optional<T> output *
	 ****************************************************************************************************/
	private JsonValue step0b(JsonValue input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 0b - NEARBY DE LIJN STOPS]");
		}
		JsonValue output = null;
		try {
			JsonValue connectionsInput = Json.objectBuilder()
	                .add("latitude", input.get("latitude")) //get(0) nakijken
	                .add("longitude", input.get("longitude"))
	                .add("range", range)
	                .build();
			if (debug) {
				System.out.println("\n[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG] ConnectionsInput: "
						+ connectionsInput);
			}
			
	        output = deLijnNearbyStops.apply(connectionsInput);
	        if (debug) {
	        	System.out.println(output);
	        }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output;
	}
	
	
	/****************************************************************************************************
	 * Step 1: Fetches the iRail NMBS connections for the given station * *
	 * 
	 * @param JsonNode
	 *            ... input *
	 * @return Optional<T> output *
	 ****************************************************************************************************/
	private JsonValue step1a(JsonValue... input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 1a - FETCHING NMBS CONNECTIONS]");
		}

		JsonValue output = null;
		if (input[0] != null) {
			output = this.fetchRailConnections(input[0].getString("name"));
		} else {
			output = this.fetchRailConnections(null);
		}

		return output;
	}
	
	/****************************************************************************************************
	 * Step 1: Fetches the iRail DeLijn connections for the given stations * *
	 * 
	 * @param JsonNode
	 *            ... input *
	 * @return Optional<T> output *
	 ****************************************************************************************************/
	private List<JsonValue> step1b(JsonValue input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 1b - FETCHING DE LIJN CONNECTIONS]");
		}

		JsonValue output = null;
		JsonArray inputArray = null;
		JsonArray outputArray = null;

		List<JsonValue> departures = new ArrayList<JsonValue>();	

		//Fetch departures for every DeLijn connection within the specified range		
		if (input.asArray().size() >0) { //is het een array
//			inputArray = input.get(0).asArray();
			inputArray=input.asArray();
			Iterator<JsonValue> iterator = inputArray.iterator();
			JsonValue jsonNode = null;
			while (iterator.hasNext()) {
				jsonNode = iterator.next();
				if (jsonNode.get("distance").asDouble() < range) {
					if (debug) {
						System.out.println(jsonNode);
					}
					output = this.fetchDeLijnConnections(jsonNode.getString("id"));
					outputArray = output.get("Departures").asArray();
					Iterator<JsonValue> departureIterator = outputArray.iterator();
					JsonValue currentJsonNode = null;
					JsonValue newNode = null;
					while (departureIterator.hasNext()) {
						currentJsonNode = departureIterator.next();

						if (debug) {
							System.out
									.println("["
											+ System.currentTimeMillis()
											+ "][ClassifierImpl][DEBUG][JSON-LD for iRAIL DeLijn CONNECTIONS][platform]"
											+ jsonNode.get("name"));
						}
						
						
						newNode = Json.objectBuilder()
								.add("platform", jsonNode.get("name"))
								.add("departureTime", currentJsonNode.get("time"))
								.add("headsign", currentJsonNode.get("short_name"))
								.add("long_name", currentJsonNode.get("long_name"))
								.add("type", currentJsonNode.get("type"))
								.add("direction", currentJsonNode.get("direction"))
								.add("iso8601", currentJsonNode.get("iso8601"))
								.build();
						departures.add(newNode);
						if (debug) {
							System.out.println("["
								+ System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][JSON-LD for iRAIL DeLijn CONNECTIONS][DepartureList]"
								+departures);
						}
					}
				}
			}
		}
		
		return departures;
	}	

	/****************************************************************************************************
	 * Step 2: Generates the JSON-LD representation of the list of iRail NMBS
	 * connections * *
	 * 
	 * @param Optional
	 *            <T> input *
	 * @return Optional<T> jsonPopulated *
	 ****************************************************************************************************/
	private List<JsonValue> step2a(JsonValue input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 2a - CONVERTING NMBS CONNECTION INTO JSON-LD]");
		}

		List<JsonValue> jsonPopulated = null;
		if (input != null) { //bestaat input
			JsonArray populatedNode = input.asArray();
			try {
				jsonPopulated = this.convertIntoJSONLDNMBS(populatedNode);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (debug) {
				System.out
						.println("["
								+ System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][JSON-LD for iRAIL CONNECTIONS]["
								+ jsonPopulated);
			}
		} else {
			if (debug) {
				System.out
				.println("["
						+ System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][NO JSON DOCUMENT AVAILABLE TO BE EXTENDED]");
			}
		}

		return jsonPopulated;
	}
	
	/****************************************************************************************************
	 * Step 2: Generates the JSON-LD representation of the list of iRail DeLijn
	 * connections * *
	 * 
	 * @param List<JsonNode> input
	 * @return Optional<T> jsonPopulated *
	 ****************************************************************************************************/
	private List<JsonValue> step2b(List<JsonValue> input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 2b - CONVERTING DE LIJN CONNECTION INTO JSON-LD]");
		}

		List<JsonValue> jsonPopulated = null;
		try {
			jsonPopulated = this.convertIntoJSONLDDeLijn(input);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (debug) {
			System.out
			.println("["
					+ System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][JSON-LD for iRAIL DeLijn CONNECTIONS]["
					+ jsonPopulated);
		}		

		return jsonPopulated;
	}	

	/****************************************************************************************************
	 * Step 3: A-Box population into the reasoning LimeDS Service * *
	 * 
	 * @param Optional
	 *            <T> input *
	 * @return ObjectNode output *
	 ****************************************************************************************************/
	private void step3(List<JsonValue> input) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 3 - POPULATING THE REASONER]");
		}

		JsonValue reasonerInput = null;

		try {
			Iterator<JsonValue> populatorIterator = input.iterator();
			while (populatorIterator.hasNext()) {
				reasonerInput = populatorIterator.next();
				if (debug) {
					System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][POPULATION OF REASONER][INPUT JSON FOR A-BOX POPULATION] "
							+ reasonerInput);
				}
				personalisedConnectionReasoner.apply(reasonerInput);
			}

			if (debug) {
				System.out
				.println("["
						+ System.currentTimeMillis()
						+ "][ClassifierImpl][INFO ][SUCCESFULLY POPULATED A-BOX][INPUT JSON FOR A-BOX POPULATION] "
						+ input);
			}
		} catch (Exception e) {
			System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][ERROR][FAILED TO POPULATE A-BOX][INPUT JSON FOR A-BOX POPULATION] "
							+ input);
			e.printStackTrace();
		}

		if (debug | saveOntology) {
			JsonObject reasonerDump = Json.objectBuilder().add("type", "dump")
					.build();
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][INPUT JSON FOR ONTOLOGY DUMP] "
						+ reasonerDump);
			}
			try {
				personalisedConnectionReasoner.apply(reasonerDump);
			} catch (Exception e) {
				System.out
						.println("["
								+ System.currentTimeMillis()
								+ "][ClassifierImpl][ERROR][FAILED TAKING ONTOLOGY DUMP] "
								+ reasonerDump);
				e.printStackTrace();
			}
		}
	}

	/****************************************************************************************************
	 * Step 4: Classification through SPARQL Querying the reasoning LimeDS
	 * Service * *
	 * 
	 * @param ObjectNode
	 *            input *
	 * @return ArrayNode output *
	 ****************************************************************************************************/
	private JsonArray step4(List<JsonValue> input,
			boolean typing) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 4a - SEMANTIC CLASSIFICATION]");
		}
		if (input != null) {
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][DEPARTURE LIST SIZE] "
						+ input.size());
			}
		}

		String headsign = null;
		JsonObject nextNode = null;
		JsonValue departure = null;
		String query = "";
		Iterator<JsonValue> iterator = input.iterator();
//		JsonNodeFactory factory = JsonNodeFactory.instance;
		JsonArray result = new JsonArray();
		JsonObject sparqlInput = null;
		JsonValue sparqlOutput = null;

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][ITERATING THROUGH ALL DEPARTURES]");
		}
		while (iterator.hasNext()) {
			nextNode = iterator.next().asObject();
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][NEXT NODE] " + nextNode);
			}
			if (!nextNode
					.toString()
					.contains(
							"http:\\/\\/users.atlantis.ugent.be\\/svrstich\\/demoOntology#currentTime")) {
				departure = nextNode.get("body");
				if (debug) {
					System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][CURRENT DEPARTURE TO BE CLASSIFIED] "
							+ departure);
				}
				headsign = departure.getString("headsign");
				if (debug) {
					System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][CONNECTION ID BEING PROCESSED] "
							+ headsign);
				}
				if (typing) {
					query = "PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
							+ "PREFIX  owl: <http://www.w3.org/2002/07/owl#> "
							+ "PREFIX  vocab: <http://vocab.org/transit/terms#> "
							+ "PREFIX  demoTLV: <http://users.atlantis.ugent.be/svrstich/demoOntology#> "
							+ "SELECT ?type WHERE {"
							+ "SubClassOf(?x, <http://vocab.org/transit/terms/Service>), "
							+ "Type(?x, ?type),"
							+ "Annotation(?x, <http://vocab.org/transit/terms/headsign>, \""+headsign+"\")"						   
						    + "}";	
					if (debug) {
						System.out.println("[" + System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][" + headsign
								+ "][QUERY STRING] " + query);
					}

					try {
						if (debug) {
							System.out.println("[" + System.currentTimeMillis()
									+ "][ClassifierImpl][DEBUG][" + headsign
									+ "][EXECUTING QUERY]");
						}
						sparqlInput = Json.objectBuilder().add("type", "get")
								.add("query", query).build();
						if (debug) {
							System.out.println("[" + System.currentTimeMillis()
									+ "][ClassifierImpl][DEBUG][" + headsign
									+ "][INPUT JSON FOR SPARQL EXECUTION] "
									+ sparqlInput);
						}
						sparqlOutput = personalisedConnectionReasoner
								.apply(sparqlInput);
						if (debug) {
							System.out.println("[" + System.currentTimeMillis()
									+ "][ClassifierImpl][DEBUG][" + headsign
									+ "][OUTPUT OF SPARQL EXECUTION] "
									+ sparqlOutput);
						}
					} catch (Exception e) {
						System.out.println("[" + System.currentTimeMillis()
								+ "][ClassifierImpl][ERROR][" + headsign
								+ "][FAILED EXECUTING SPARQL QUERY] "
								+ sparqlInput);
						e.printStackTrace();
					}

					if (debug) {
						System.out.println("[" + System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][" + headsign
								+ "][ADDING NODE IN RESULTSET] " + sparqlInput);
					}
				}

				if (debug) {
					System.out.println("DEPARTURE PLATFORM::" + departure.get("platform"));
					System.out.println("DEPARTURE PLATFORM TEXTVALUE::" + departure.get("platform").getString("@id"));
				
					System.out.println("DEPARTURE DELAY::" + departure.get("hasDelay"));
				}
				// Iterator through the result set and set the types of every
				// connection
				JsonObjectBuilder reasonerOutputBuilder = Json.objectBuilder();
				reasonerOutputBuilder.add("delay", departure.get("hasDelay"));
				reasonerOutputBuilder.add("platform", departure.get("platform").getString("@id"));
				reasonerOutputBuilder.add("headsign", departure.getString("headsign"));
				reasonerOutputBuilder.add("departureTime",
						departure.getLong("departureTime"));
				reasonerOutputBuilder.add("direction",
						departure.getString("direction"));
				reasonerOutputBuilder.add("type", departure.getString("trainType"));
				

				if (typing) {
					// Add all types for the connection
					// reasonerOutputBuilder.add("types", Json.arrayOf("1", "2",
					// "3", "4", "5", "6", "7", "8", "9", "10"));
					JsonArray array = new JsonArray();
					Iterator<String> typeIterator = analyseClassifiedConnectionTypes(
							sparqlOutput).iterator();
					String type = null;
					while (typeIterator.hasNext()) {
						type = typeIterator.next();
						if (Categories.getNumericValueForString(type) != 0) {
							array.add(Categories.getNumericValueForString(type));
						}
					}

					if (debug) {
						System.out
						.println(analyseClassifiedConnectionTypes(sparqlOutput));
					}
					JsonObject node = reasonerOutputBuilder.add("types", array)
							.build();

				}
				
				result.add(reasonerOutputBuilder.build());
			}
		}

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][FINAL RESULTSET] " + result);
		}

		return result;
	}

	private JsonObject step4(String train, boolean typing) {
		if (info) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][INFO ][STEP 4b - SEMANTIC REALIZATION]");
		}

		if (train != null) {
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][BUS TO BE CLASSIFIED] "
						+ train);
			}
		}

		if (train.startsWith("\"")) {
			train = train.substring(1, train.length() - 1);
		}

		String query = "";
		JsonObject sparqlInput = null;
		JsonValue sparqlOutput = null;
		JsonObject node = null;

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][CONNECTION ID BEING PROCESSED] "
					+ train);
		}
		if (typing) {
			query = "PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
					+ "PREFIX  owl: <http://www.w3.org/2002/07/owl#> "
					+ "PREFIX  vocab: <http://vocab.org/transit/terms#> "
					+ "PREFIX  demoTLV: <http://users.atlantis.ugent.be/svrstich/demoOntology#> "
					+ "SELECT ?type WHERE {"
					+ "SubClassOf(?x, <http://vocab.org/transit/terms/Service>), "
					+ "Annotation(?x, <http://vocab.org/transit/terms/headsign>, \""+train+"\"),"
				    + "Type(?x, ?type)"
				    + "}";	
			
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][" + train + "][QUERY STRING] "
						+ query);
			}

			try {
				if (debug) {
					System.out.println("[" + System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][" + train
							+ "][EXECUTING QUERY]");
				}
				sparqlInput = Json.objectBuilder().add("type", "get")
						.add("query", query).build();
				if (debug) {
					System.out.println("[" + System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][" + train
							+ "][INPUT JSON FOR SPARQL EXECUTION] " + sparqlInput);
				}
				sparqlOutput = personalisedConnectionReasoner
						.apply(sparqlInput);
				if (debug) {
					System.out.println("[" + System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][" + train
							+ "][OUTPUT OF SPARQL EXECUTION] " + sparqlOutput);
				}
			} catch (Exception e) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][ERROR][" + train
						+ "][FAILED EXECUTING SPARQL QUERY] " + sparqlInput);
				e.printStackTrace();
			}

			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][" + train
						+ "][ADDING NODE IN RESULTSET] " + sparqlInput);
			}
		}

		// Iterator through the result set and set the types of every connection
		JsonObjectBuilder reasonerOutputBuilder = Json.objectBuilder();
		if (typing) {
			// Add all types for the connection
			// reasonerOutputBuilder.add("types", Json.arrayOf("1", "2", "3",
			// "4", "5", "6", "7", "8", "9", "10"));
//			JsonNodeFactory factory = JsonNodeFactory.instance;
			JsonArray array = new JsonArray();
			Iterator<String> typeIterator = analyseClassifiedConnectionTypes(
					sparqlOutput).iterator();
			String type = null;
			while (typeIterator.hasNext()) {
				type = typeIterator.next();
				if (Categories.getNumericValueForString(type) != 0) {
					array.add(Categories.getNumericValueForString(type));
				}
			}

			if (debug) {
				System.out.println(analyseClassifiedConnectionTypes(sparqlOutput));
			}
			node = reasonerOutputBuilder.add("types", array).build();
		}

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][FINAL RESULTSET] " + node);
		}

		return node;
	}

	/****************************************************************************************************
	 * LIMEDS: PRIVATE SUPPORT-METHODS *
	 * 
	 * @param <T>
	 ****************************************************************************************************/

	private List<String> analyseClassifiedConnectionTypes(
			JsonValue resultSet) {
		List<String> types = new ArrayList<>();
		JsonValue result = resultSet.get("results").get("bindings");
		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][RESULTSET] " + result);
		}
		
		//JsonArray graph = result.get("@graph").asArray();
		JsonArray graph=result.asArray();

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][@GRAPH] " + graph);
		}		
		
		Iterator<JsonValue> iterator = graph.iterator();
		JsonValue element = null;
		while (iterator.hasNext()) {
			element = iterator.next();
			
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][ELEMENT] " + element);
			}					
			
			if (element.has("type")) {

				if (debug) {
					System.out.println("[" + System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][RS:VALUE] " + element.get("rs:value"));
				}					

				JsonValue type = element.get("type");
				
				if (debug) {
					System.out.println("[" + System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][TYPE] " + type);
				}									
				
				if (debug) {
					System.out.println("[" + System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][@ID] " + type.getString("@id"));
				}									
				
				types.add(type.getString("value"));				
				
			}
		}

		return types;
	}
	
	/****************************************************************************************************
	 * This method contacts the iRailDeLijnConnections LimeDS service to fetch all
	 * DeLijn Connection for * the given location * *
	 * 
	 * @param stopid
	 *            - The id of the DeLijn stop to be queried *
	 * @return JSON with all connections for the given stop, incl. some
	 *         additional context * information *
	 ****************************************************************************************************/
	private JsonValue fetchDeLijnConnections(String stopID) {
		long unixTime = (System.currentTimeMillis() / 1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy 'om' HH:mm a");
		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][CURRENT TIMESTAMP] "
					+ unixTime);
		}
		
		JsonObject connectionsInput = Json.objectBuilder()
				.add("id", stopID)
				.add("time", unixTime+3600).build();		
		JsonValue output = null;
		
		if (debug) {
			System.out
			.println("["
					+ System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][INPUT JSON FOR DE LIJN CONNECTIONS FETCH] "
					+ connectionsInput);
		}

		try {
			if (debug) {
				System.out.println("[REQUESTING DELIJN IRAIL CONNECTION AS FROM] " + sdf.format(unixTime*1000L));
			}
			output = deLijnDepartures.apply(connectionsInput);
		} catch (Exception e) {
			System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][ERROR][FAILED TO FETCH DE LIJN CONNECTIONS FOR "
							+ stopID + "] " + connectionsInput);
			e.printStackTrace();
		}

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][De LIJN CONNECTIONS]["
					+ ((output.get(0).asArray())).size() + "] " + output);
		}
		return output;		
		
	}

	/****************************************************************************************************
	 * This method contacts the iRailNMBSConnections LimeDS service to fetch all
	 * Rail Connection for * the given location * *
	 * 
	 * @param stationName
	 *            - The name of the railway station to be queried *
	 * @return JSON with all connections for the given station, incl. some
	 *         additional context * information *
	 ****************************************************************************************************/
	private JsonValue fetchRailConnections(
			String stationName) {

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][STATIONNAME][DEBUG] "
					+ stationName);
		} else {
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][STATIONNAME][OPERATIONAL] "
						+ stationName);
			}
		}

		if (!stationName.startsWith("BE.NMBS")) {
			if (debug) {
				System.out
						.println("["
								+ System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][STATION NAME NEEDS TO BE CONVERTED]");
			}
			stationName = this.convertLexicalStationNameIntoID(stationName);
		}

		long unixTime = System.currentTimeMillis()/1000L;
		SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy 'om' HH:mm a");
		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][CURRENT TIMESTAMP] "
					+ unixTime);
		}

		JsonObject connectionsInput = Json.objectBuilder()
				.add("id", stationName)
				.add("time", unixTime+(3600*0)).build();

		JsonValue output = null;

		if (debug) {
			System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][DEBUG][INPUT JSON FOR RAIL CONNECTIONS FETCH] "
							+ connectionsInput);
		}

		try {
			if (debug) {
				System.out.println("[REQUESTING NMBS IRAIL CONNECTION AS FROM] " + sdf.format(unixTime*1000L));
			}
			output = iRailNMBSConnections.apply(connectionsInput);
		} catch (Exception e) {
			System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][ERROR][FAILED TO FETCH RAIL CONNECTIONS FOR "
							+ stationName + "] " + connectionsInput);
			e.printStackTrace();
		}

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][iRAIL CONNECTIONS]["
					+ ((JsonArray) (output.asArray())).size() + "] " + output);
		}
		return output;
	}

	/****************************************************************************************************
	 * This method extends a given JSON String with the necessary concepts to
	 * make * it JSON-LD compliant according to the TraPIST ontology * *
	 * 
	 * @param populatedNode
	 *            - The JsonNode to be extended *
	 * @return JSON-LD with all connections for the given station, incl. some
	 *         additional context * information
	 * @throws Exception
	 *             *
	 ****************************************************************************************************/
	private  List<JsonValue> convertIntoJSONLDNMBS(
			JsonValue populatedNode) throws Exception {
		List<JsonValue> toBeInserted = new ArrayList<JsonValue>();

	JsonObject result = Json
				.objectBuilder()
				.add("@type",
						"http://users.atlantis.ugent.be/svrstich/demoOntology#ConnectionList")
				.add("@id",
						"http://users.atlantis.ugent.be/svrstich/demoOntology#connectionListIndividual")
				.add("@context",
						Json.objectBuilder()
								.add("departures",
										Json.objectBuilder()
												.add("@id",
														"http://users.atlantis.ugent.be/svrstich/demoOntology#departures")
												.build()).build())
				.add("@id",
						"http://users.atlantis.ugent.be/svrstich/demoOntology#connectionListIndividual")
				// .add("departures", populatedNode)
				.build();

		// toBeInserted.add(result);

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][JSON-LD INITIAL HEADER] "
					+ result);
		}

		JsonArray departuresArray = populatedNode.asArray();
		Iterator<JsonValue> departuresIterator = departuresArray.iterator(); 
		JsonValue departure = null;

		while (departuresIterator.hasNext()) {
			departure = departuresIterator.next();
			toBeInserted.add(getJSONNMBSConnection(departure));
		}

		toBeInserted.add(this.getCurrentJSONTimeStamp());

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][JSON-LD @CONTEXT BLOCK] "
					+ toBeInserted);
		}

		return toBeInserted;
	}

	/****************************************************************************************************
	 * This method extends a given JSON String with the necessary concepts to
	 * make * it JSON-LD compliant according to the TraPIST ontology * *
	 * 
	 * @param populatedNode
	 *            - The JsonNode to be extended *
	 * @return JSON-LD with all connections for the given station, incl. some
	 *         additional context * information
	 * @throws Exception
	 *             *
	 ****************************************************************************************************/
	private List<JsonValue> convertIntoJSONLDDeLijn(
			List<JsonValue> populatedNode) throws Exception {
		List<JsonValue> toBeInserted = new ArrayList<JsonValue>();

		JsonObject result = Json
				.objectBuilder()
				.add("@type",
						"http://users.atlantis.ugent.be/svrstich/demoOntology#ConnectionList")
				.add("@id",
						"http://users.atlantis.ugent.be/svrstich/demoOntology#connectionListIndividual")
				.add("@context",
						Json.objectBuilder()
								.add("departures",
										Json.objectBuilder()
												.add("@id",
														"http://users.atlantis.ugent.be/svrstich/demoOntology#departures")
												.build()).build())
				.add("@id",
						"http://users.atlantis.ugent.be/svrstich/demoOntology#connectionListIndividual")
				// .add("departures", populatedNode)
				.build();

		//toBeInserted.add(result);

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][JSON-LD INITIAL HEADER] "
					+ result);
		}

		Iterator<JsonValue> departuresIterator = populatedNode.iterator();
		JsonValue departure = null;

		while (departuresIterator.hasNext()) {
			departure = departuresIterator.next();
			toBeInserted.add(getJSONDeLijnConnection(departure));
		}

		//toBeInserted.add(this.getCurrentJSONTimeStamp());

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][JSON-LD @CONTEXT BLOCK] "
					+ toBeInserted);
		}

		return toBeInserted;
	}
	
	
	private JsonObject getCurrentJSONTimeStamp() {
		long unixTime = (System.currentTimeMillis() / 1000L);
		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][CURRENT UNIX TIME] " 
					+ unixTime);
		}
		
		unixTime = unixTime + (3600*0);
		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][ENHANCED CURRENT UNIX TIME] " 
					+ unixTime);
		}		
		
		JsonObject result = Json
				.objectBuilder()
				.add("type", "add")
				.add("body",
						Json.objectBuilder()
								.add("@context",
										Json.objectBuilder()
												.add("currentTime",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#currentTime")
																//.add("@type",
																		//"http://www.w3.org/2001/XMLSchema#long")
																.build())
												.build())
								.add("@id",
										"http://users.atlantis.ugent.be/svrstich/demoOntology#nowIndividual")
								.add("currentTime",
										unixTime)
								.build()).build();
		if (debug) {
			System.out.println(result);
		}
		return result;
	}

	private JsonObject getJSONNMBSConnection(JsonValue departure) {
		String label = departure.getString("label").replaceAll(" ", "_");

		//System.out.println("[XXX]" + departure.get("delay").getClass());
		//System.out.println("[YYY]" + departure.get("delay").getLongValue());
		
		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][LABEL] " + label);
		}

		//Since there is no information on the amount of places available, we emulate this with a chance of 1 in 4  
		//not to have any places available anymore for luggage/parcels/bicycles.
		Random rg = new Random();
		
		JsonObject result = Json
				.objectBuilder()
				.add("type", "add")
				.add("body",
						Json.objectBuilder()
								.add("@context",
										Json.objectBuilder()
												.add("hasDelay",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#hasDelay")
																//.add("@type",
																	//	"http://www.w3.org/2001/XMLSchema#long")
																.build())
												.add("platform",
														Json.objectBuilder()
																.add("@id",
																		//"http://users.atlantis.ugent.be/svrstich/demoOntology#hasPlatform")
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#departsFromPlatform")
																//.add("@type",
																	//	"http://www.w3.org/2001/XMLSchema#string")
																.build())
												.add("headsign",
														Json.objectBuilder()
																.add("@id",
																		"http://vocab.org/transit/terms/headsign")
																.build())
												.add("departureTime",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#departureTime")
																//.add("@type",
																	//	"http://www.w3.org/2001/XMLSchema#long")
																.build())
												.add("direction",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#hasDirection")
																//.add("@type",
																		//"http://www.w3.org/2001/XMLSchema#string")
																.build())
												.add("trainType",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#trainType")
																.build())
												.add("placesAvailable",
														Json.objectBuilder()
																.add("@id", 
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#placesAvailable")
																.build())
												.build())
								.add("@id",
										"http://users.atlantis.ugent.be/svrstich/demoOntology#"
												+ label)
								.add("@type",
										"http://users.atlantis.ugent.be/svrstich/demoOntology#"
												+ departure.getString("id"))
								//.add("hasDelay", departure.get("delay"))
								.add("hasDelay", Long.parseLong(departure.getString("delay")))
								.add("platform",
										Json.objectBuilder()
										.add("@id",
												"http://users.atlantis.ugent.be/svrstich/demoOntology#NMBS_Platform_"+stationName+"_"+(departure.getString("platform").replace("\"",""))).build())
										//"[NMBS] Platform " + departure.get("platform")
												//+ "")
								.add("headsign", departure.get("id"))
								.add("departureTime",
										departure.get("departureTime"))
								.add("direction", departure.get("direction"))
								.add("trainType", TypeResolver.getInstance().getString(departure.getString("type")))
								.add("placesAvailable", rg.nextInt(20))
								.build()).build();

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][DEPARTURE] " + result);
		}
		return result;

	}
	
	private JsonObject getJSONDeLijnConnection(JsonValue departure) {
		String label = departure.getString("long_name").replaceAll(" ", "_");
		String platform = "";
		if (departure.get("platform") != null) {
			platform = departure.getString("platform");
		}

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][LABEL] " + label);
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][DELAY] " + departure.get("delay"));
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][PLATFORM] " + departure.get("platform"));
		}
		
		long delay = 0;
		if (departure.get("delay") != null) {
			delay = departure.getLong("delay");
		}

		JsonObject result = Json
				.objectBuilder()
				.add("type", "add")
				.add("body",
						Json.objectBuilder()
								.add("@context",
										Json.objectBuilder()
												.add("hasDelay",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#hasDelay")
																//.add("@type",
																	//	"http://www.w3.org/2001/XMLSchema#long")
																.build())
												.add("platform",
														Json.objectBuilder()
																.add("@id",
																		//"http://users.atlantis.ugent.be/svrstich/demoOntology#hasPlatform")
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#departsFromPlatform")
																.build())
												.add("headsign",
														Json.objectBuilder()
																.add("@id",
																		"http://vocab.org/transit/terms/headsign")
																.build())
												.add("departureTime",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#departureTime")
																//.add("@type",
																	//	"http://www.w3.org/2001/XMLSchema#long")
																.build())
												.add("direction",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#hasDirection")
																//.add("@type",
																		//"http://www.w3.org/2001/XMLSchema#string")
																.build())
												.add("trainType",
														Json.objectBuilder()
																.add("@id",
																		"http://users.atlantis.ugent.be/svrstich/demoOntology#trainType")
																.build())
												.build())
								.add("@id",
										"http://users.atlantis.ugent.be/svrstich/demoOntology#"
												+ departure.getString("headsign") + "_" + departure.getString("direction") + "_" + departure.getString("iso8601"))
								.add("@type",
										"http://users.atlantis.ugent.be/svrstich/demoOntology#"
												+ "Series_DELIJN_" + departure.getString("headsign") + "_" + departure.getString("direction"))
								//.add("hasDelay", departure.get("delay"))
								.add("hasDelay", delay)
								.add("platform",
										//"[DE LIJN] " + departure.get("platform").asText().replaceAll("\"", " "))
										Json.objectBuilder()
										.add("@id",
												"http://users.atlantis.ugent.be/svrstich/demoOntology#DELIJN_"+platform.replace("\"","").replace(" ", "_")).build())
										
								.add("headsign", departure.getString("headsign") +"_"+System.currentTimeMillis())
								.add("departureTime",
										departure.get("departureTime").asLong()+3600)
								.add("direction", label)
								.add("trainType", TypeResolver.getInstance().getString(departure.getString("type")))
								.build()).build();
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][DEPARTURE] " + result);
		}
		return result;

	}	

	/****************************************************************************************************
	 * This method contacts the iRail API in order to convert a lexical
	 * stationname into * an iRail ID, e.g. Zingem -> BE.NMBS.008892635 * *
	 * 
	 * @param stationName
	 *            - The stationname to be converted *
	 * @return station ID *
	 ****************************************************************************************************/
	private <T extends JsonValue> String convertLexicalStationNameIntoID(
			String stationName) {

		JsonValue stationsInput = Json.objectBuilder()
				.add("name", stationName).build();

		if (debug) {
			System.out.println("[" + System.currentTimeMillis()
					+ "][ClassifierImpl][DEBUG][INPUT FOR CONVERSION REQUEST] "
					+ stationsInput);
		}

		try {
			JsonValue stationsOutput = iRailNMBSStations
					.apply(stationsInput);
			if (debug) {
				System.out
						.println("["
								+ System.currentTimeMillis()
								+ "][ClassifierImpl][DEBUG][OUTPUT FROM CONVERSION REQUEST] "
								+ stationsOutput);
			}

//			JsonArray node = (JsonArray) stationsOutput.get(0);
			JsonValue node=stationsOutput.get(0);


			String stationID = node.getString("id");
			if (debug) {
				System.out.println("[" + System.currentTimeMillis()
						+ "][ClassifierImpl][DEBUG][STATION ID] " + stationID);
			}
			return stationID;

		} catch (Exception e) {
			System.out
					.println("["
							+ System.currentTimeMillis()
							+ "][ClassifierImpl][ERROR][FAILED TO CONVERT STATIONNAME INTO STATION ID "
							+ stationName + "] " + stationName);
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	

}
