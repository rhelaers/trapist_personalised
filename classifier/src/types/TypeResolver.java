package types;

import java.util.HashMap;
import java.util.Map;

public class TypeResolver {
	
	private Map<String, String> types = null;
	private static TypeResolver instance = null;

	public TypeResolver() {
		types = new HashMap<>();
		
		types.put("3", "BUS");
	}
	
	
	public String getString(String type) {
		if (types.containsKey(type)) {
			return types.get(type);
		} else {
			return type;
		}
	}
	
	public synchronized static final TypeResolver getInstance() {
		if (instance == null) {
			instance = new TypeResolver();
		}
		return instance;
	}
}