package geo;

public class GeoTools {
	public final static double AVERAGE_RADIUS_OF_EARTH = 6371;

	/**
	 * Calculate the distance between two lat lng points, using the great-circle
	 * distance haversine formula.
	 * 
	 * @param lat1
	 *            'From'-latitude
	 * @param lng1
	 *            'From'-longitude
	 * @param lat2
	 *            'To'-latitude
	 * @param lng2
	 *            'To'-longitude
	 * @return The straight distance between (lat1,lng1) and (lat2,lng2) in km
	 */
	public static double getDistance(double lat1, double lng1, double lat2,
			double lng2) {
		double latDistance = Math.toRadians(lat2 - lat1);
		double lngDistance = Math.toRadians(lng2 - lng1);

		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lngDistance / 2)
				* Math.sin(lngDistance / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return (double) AVERAGE_RADIUS_OF_EARTH * c;
	}
}
