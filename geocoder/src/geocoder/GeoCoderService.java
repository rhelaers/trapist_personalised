package geocoder;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;

@Validated(input=false,output=false)
@Segment(id = "GoogleGeocoder", tags = "producer")
public class GeoCoderService implements FunctionalSegment {

	private static final String API_KEY = "AIzaSyDAtaEDA1-FL8s5PzC8WnTgH45f3sPx6zI";
	private static final String API_ENDPOINT = "https://maps.googleapis.com/maps/api/geocode/json";

	@Service
	private Client httpClient;
	
	//@InputDoc(schemaRef="geocoder.model.GeoCoderService_input_0.1.0")
	@OutputDoc(schemaRef="geocoder.model.GeoCoderService_output_0.1.0")
	@HttpOperation(method = HttpMethod.GET, path = "/tools/geocode/{addressLine}")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		String addressLine;
		if(input[1].has("request")){
			addressLine = input[1].get("request").get("path").getString("addressLine");
		}
		else{
			addressLine = input[1].get("path").getString("addressLine");
		}

		ClientResponse<JsonValue> response = httpClient.target(API_ENDPOINT)
				.withQuery("address", addressLine).withQuery("key", API_KEY)
				.get().returnObject(Json.getDeserializer());
		if (response.status() == 200) {
			JsonValue result = response.getBody();
			double latitude = result.get("results").get(0).get("geometry")
					.get("location").get("lat").asDouble();
			double longitude = result.get("results").get(0).get("geometry")
					.get("location").get("lng").asDouble();

			JsonValue location = Json
					.objectBuilder()
					.add("label", addressLine)
					.add("latitude", latitude)
					.add("longitude", longitude)
					.add("mapsLink",
							"http://maps.google.com/maps?q=loc:" + latitude
									+ "," + longitude).build();
			return location;
		} else {
			throw new Exception("Invalid response!");
		}
	}

}
