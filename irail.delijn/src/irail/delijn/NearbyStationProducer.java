package irail.delijn;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonObjectBuilder;
import org.ibcn.limeds.json.JsonValue;

import geo.GeoTools;
import tools.irail.IRail;

@Validated(input=false,output=false)
@Segment(id = "be.irail.delijn.nearby-stations", tags = { 
		"delijn", "irail" })
public class NearbyStationProducer implements FunctionalSegment {

	@Service
	private Client httpClient;
	
	@InputDoc(schemaRef="irail.delijn.model.NearbyStationProducer_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/irail/delijn/nearby-stations")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
//		IRailDelijn irail = new IRailDelijn();
		IRail irail = new IRail();
		double lat = input[0].getDouble("latitude");
		double lng = input[0].getDouble("longitude");
		double range = input[0].getDouble("range");
		JsonArray stations =irail.getStationsDeLijn(httpClient, null).asArray();
		JsonArray resultaat=new JsonArray();
		for(int i=0; i<stations.size(); i++){
			JsonValue v=addDistance(stations.get(i), lat, lng);
			if(v.getDouble("distance")<= range){
				resultaat.add(v);
			}
		}
		resultaat.sort(this::byRangeDesc);
		
		return resultaat;
	}
	
	private int byRangeDesc(JsonValue o1, JsonValue o2) {
		return Double.compare(o1.getDouble("distance"),
				o2.getDouble("distance"));
	}

	
	private JsonValue addDistance(JsonValue node, double lat, double lng) {
		double ulat = node.get("latitude").asDouble();
		double ulng = node.get("longitude").asDouble();
		double distance = GeoTools.getDistance(ulat, ulng, lat, lng);
		((JsonObject) node).put("distance", distance);
		return node;
	}

}
