package irail.delijn;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.JsonValue;

import tools.irail.IRail;

@Segment(id = "be.irail.delijn.stations", tags = { 
		"delijn", "irail" })
public class StationsProducer implements FunctionalSegment {

	@Service
	private Client httpClient;
	
	@HttpOperation(method = HttpMethod.GET, path = "/irail/delijn/stations")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		IRail irail = new IRail();
		return irail.getStationsDeLijn(httpClient, null);
	}
	

	
	

}
