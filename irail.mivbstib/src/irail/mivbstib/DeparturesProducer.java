package irail.mivbstib;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonValue;

import tools.irail.IRail;

@Validated(input=false,output=false)
@Segment(id = "be.irail.mivbstib.departures", tags = { 
		"mivbstib", "irail" })
public class DeparturesProducer implements FunctionalSegment {

	@Service
	private Client httpClient;
	
	@InputDoc(schemaRef="irail.mivbstib.model.DeparturesProducer_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/irail/mivbstib/departures")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		IRail irail = new IRail();
		JsonValue in = input[0];
		LocalDateTime date = in.has("time") ? LocalDateTime.ofEpochSecond(in
				.get("time").asLong(), 0, ZoneOffset.ofHours(2)) : LocalDateTime.now();
		if (in.has("id") || in.has("name")) {
			return irail.getDeparturesMIVBSTIB(httpClient, in, date);
		} else {
			throw new Exception("No results found");
		}
	}
	


}
