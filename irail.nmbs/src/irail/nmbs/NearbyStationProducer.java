package irail.nmbs;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

import geo.GeoTools;
import tools.irail.IRail;

@Validated(input=false,output=false)
@Segment(id = "be.irail.nmbs.nearby-stations", tags = { 
		"nmbs", "irail" })
public class NearbyStationProducer implements FunctionalSegment {
	private static final String ID_PREFIX = "BE.NMBS.";
	
	@Service
	private Client httpClient;

	@InputDoc(schemaRef="irail.nmbs.model.NearbyStationProducer_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/irail/nmbs/nearby-stations")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		IRail irail = new IRail();
		double lat = input[0].get("location").getDouble("Latitude");
		double lng = input[0].get("location").getDouble("Longitude");
		int range = input[0].getInt("range");
		JsonArray stations =irail.getStationsNMBS(httpClient, null).asArray();
		JsonArray resultaat=new JsonArray();
		for(int i=0; i<stations.size(); i++){
			JsonValue v=addDistance(stations.get(i), lat, lng);
			if(v.getInt("distance")<= range){
				JsonValue val = transformNode(v);
				resultaat.add(val);
			}
		}
		resultaat.sort(this::byRangeDesc);
		
		return resultaat;
	}
	
	private int byRangeDesc(JsonValue o1, JsonValue o2) {
		return Double.compare(o1.getDouble("distance"),
				o2.getDouble("distance"));
	}

	
	private JsonValue addDistance(JsonValue node, double lat, double lng) {
		double ulat = node.get("latitude").asDouble();
		double ulng = node.get("longitude").asDouble();
		double distance = GeoTools.getDistance(ulat, ulng, lat, lng);
		((JsonObject) node).put("distance", distance);
		return node;
	}
	
	private JsonValue transformNode(JsonValue node) {
		double distance = node.getDouble("distance");
		((JsonObject) node).remove("distance");
		return Json.objectBuilder().add("station", formatStation(node))
				.add("distance", distance).build();
	}

	

	private JsonValue formatStation(JsonValue orig) {
		JsonArray alternatives=new JsonArray();
		if(orig.has("alternative")){
			JsonArray alt=orig.get("alternative").asArray();
			for(int i=0; i<alt.size(); i++){
				JsonValue v=Json.objectBuilder().add("language", alt.get(i).getString("@language"))
						.add("name", alt.get(i).getString("@value")).build();
				alternatives.add(v);
			}
		}
		
		return Json
				.objectBuilder()
				.add("id", parseId(orig.getString("@id")))
				// TODO
				.add("name", orig.getString("name"))
				.add("alternativeNames", alternatives)
				.add("location",
						Json.objectBuilder()
								.add("Longitude",
										orig.get("longitude").asDouble())
								.add("Latitude",
										orig.get("latitude").asDouble()))
				.build();
		
	}
	private String parseId(String idUri) {
		return ID_PREFIX + idUri.substring(idUri.lastIndexOf("/") + 1);
	}


}
