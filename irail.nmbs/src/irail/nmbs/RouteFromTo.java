package irail.nmbs;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

import tools.irail.IRail;

@Validated(input=false,output=false)
@Segment(id="be.irail.nmbs.routeFromTo", tags = { 
		"nmbs", "irail" })
public class RouteFromTo implements FunctionalSegment{
	
	@Service
	private Client httpClient;
	
	@InputDoc(schemaRef="irail.nmbs.model.RouteFromTo_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/irail/nmbs/routeFromTo")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		
		IRail iRail = new IRail();
		JsonValue in = input[0];
		LocalDateTime date = in.has("time") ? LocalDateTime.ofEpochSecond(in
						.get("time").asLong(), 0, ZoneOffset.ofHours(2)) : LocalDateTime.now();
				if (in.has("idFrom") && in.has("idTo")) {
					StringBuilder builder=new StringBuilder();
					builder.append("to=").append(in.getString("idTo").replace(" ", "-")).append("&from=").append(in.getString("idFrom").replace(" ", "-"));
					String dag=date.format(DateTimeFormatter.ofPattern("ddMMyy"));
					builder.append("&date=").append(dag);
					String tijd=date.format(DateTimeFormatter.ofPattern("HHmm"));
					builder.append("&time=").append(tijd);					
					if(in.has("timeSel")){
						builder.append("&timeSel=").append(in.getString("timeSel"));
					}
					builder.append("&format=json");
					
					JsonValue node = iRail.getRoutesFromTo(httpClient, builder.toString());	
					
					JsonArray array=node.get("connection").asArray();
					for(int i=0; i< array.size(); i++){
						array.set(i, transform(array.get(i).get("departure")));
					}
					return array;
					
				} else {
					throw new Exception("No results found");
				}
				
	}
	
	private String getType(String label) {
		String laatste=label.trim().split("\\.")[2];
		return laatste.replaceAll("[^A-Za-z]","");
	}
	
	private String getHeadSign(String vehicle){
		if(vehicle.startsWith("BE.NMBS.IC") ){
			return vehicle.trim().substring(10);
		}
		else if(vehicle.startsWith("BE.NMBS.L") | vehicle.startsWith("BE.NMBS.P")){
			return vehicle.trim().substring(9);
		}
		else if(vehicle.startsWith("BE.NMBS.EUR") | vehicle.startsWith("BE.NMBS.EXT") | vehicle.startsWith("BE.NMBS.ICE") | vehicle.startsWith("BE.NMBS.THA")){
			return vehicle.trim().substring(11);
		}
		return vehicle;
	}

	private JsonValue transform(JsonValue node) {
		JsonObject obj=Json.objectBuilder()
				.add("id", getHeadSign(node.getString("vehicle")))
				.add("delay", node.getString("delay"))
				.add("platform", node.getString("platform"))
				.add("departureTime", node.getString("time"))
				.add("direction", node.get("direction").getString("name")) 
				.add("type", getType(node.getString("vehicle")))
				.add("label", getHeadSign(node.getString("vehicle")))
				.build();
		return (JsonValue) obj;

	}


}
