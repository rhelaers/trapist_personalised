package irail.nmbs;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;

import tools.irail.IRail;

@Segment(id="be.irail.nmbs.stations.helper", tags = { 
		"nmbs", "irail" })
public class StationsHelper implements FunctionalSegment {

	private static final String ID_PREFIX = "BE.NMBS.";
	private static final String ENDPOINT_URL="https://irail.be/stations/NMBS";
	
	@Service
	private Client httpClient;
	
	private JsonArray getStations(String query) throws Exception {
		ClientResponse<JsonValue> response = httpClient.target(ENDPOINT_URL)
				.withHeader("Accept", "application/json").withQuery("q", query)
				.get().returnObject(Json.getDeserializer());
		return  (JsonArray) response.getBody().get("@graph");
	}

	private String parseId(String idUri) {
		return ID_PREFIX + idUri.substring(idUri.lastIndexOf("/") + 1);
	}
	
	@HttpOperation(method = HttpMethod.POST, path = "/irail/nmbs/stations/helper")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		JsonArray result = new JsonArray();
		if (input[0].has("id")) {
			String processedId = extractId(input[0].getString("id"));
			JsonArray stations=getStations("").asArray();
			for(int i=0; i<stations.size(); i++){
				if(stations.get(i).getString("@id").endsWith(processedId)){
					JsonValue v=formatStation(stations.get(i));
					result.push(v);
				}
			}			
			
		} else if (input[0].has("name")) {
			String processedName = extractNameQuery(input[0].getString("name"));
			JsonArray respons= getStations(processedName).asArray();
			for(int i=0; i<respons.size(); i++){
				JsonValue v=formatStation(respons.get(i));
				result.push(v);
			}
			
		} else {
			JsonArray respons= getStations("").asArray();
			for(int i=0; i<respons.size(); i++){
				JsonValue v=formatStation(respons.get(i));
				result.push(v);
			}
		}
		return result;
	}
	
	private String extractId(String id) {
		id = id.trim();
		if (id.startsWith(ID_PREFIX)) {
			return id.replace(ID_PREFIX, "");
		}
		return id;
	}

	private String extractNameQuery(String name) {
		name = name.trim().toLowerCase();
		if (name.contains("[")) {
			return name.substring(0, name.indexOf('[')).trim();
		}
		return name;
	}
	
	
	
	

	private JsonValue formatStation(JsonValue orig) {
		JsonArray alternatives=new JsonArray();
		if(orig.has("alternative")){
			JsonArray alt=orig.get("alternative").asArray();
			for(int i=0; i<alt.size(); i++){
				JsonValue v=Json.objectBuilder().add("language", alt.get(i).getString("@language"))
						.add("name", alt.get(i).getString("@value")).build();
				alternatives.add(v);
			}
		}
		
		return Json
				.objectBuilder()
				.add("id", parseId(orig.getString("@id")))
				// TODO
				.add("name", orig.getString("name"))
				.add("alternativeNames", alternatives)
				.add("location",
						Json.objectBuilder()
								.add("Longitude",
										orig.get("longitude").asDouble())
								.add("Latitude",
										orig.get("latitude").asDouble()))
				.build();
		
	}


}
