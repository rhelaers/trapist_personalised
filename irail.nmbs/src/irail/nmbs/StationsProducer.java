package irail.nmbs;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;
import tools.irail.IRail;

@Validated(input=false,output=false)
@Segment(id = "be.irail.nmbs.stations", tags = { "nmbs",
		"irail" })
public class StationsProducer implements FunctionalSegment {
	
	private static final String ID_PREFIX = "BE.NMBS.";
	
	@Service
	private Client httpClient;
	
	@InputDoc(schemaRef="irail.nmbs.model.StationsProducer_input_0.1.0")
	@OutputDoc(schemaRef="irail.nmbs.model.StationsProducer_output_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/irail/nmbs/stations")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		IRail irail = new IRail();
		JsonArray result = new JsonArray();
		if (input[0].has("id")) {
			String processedId = extractId(input[0].getString("id"));
			JsonArray stations=irail.getStationsNMBS(httpClient, null).asArray();
			for(int i=0; i<stations.size(); i++){
				if(stations.get(i).getString("@id").endsWith(processedId)){
					JsonValue v=formatStation(stations.get(i));
					result.push(v);
				}
			}			
		}
		else if (input[0].has("name")) {
			String name = extractNameQuery(input[0].getString("name"));
			JsonArray respons= irail.getStationsNMBS(httpClient, name).asArray();
			for(int i=0; i<respons.size(); i++){
				JsonValue v=formatStation(respons.get(i));
				result.push(v);
			}			
		}
		else{
			JsonArray respons= irail.getStationsNMBS(httpClient, null).asArray();
			for(int i=0; i<respons.size(); i++){
				JsonValue v=formatStation(respons.get(i));
				result.push(v);
			}			
		}
		return result;
	}

	private JsonValue formatStation(JsonValue orig) {
		JsonArray alternatives=new JsonArray();
		if(orig.has("alternative")){
			JsonArray alt=orig.get("alternative").asArray();
			for(int i=0; i<alt.size(); i++){
				JsonValue v=Json.objectBuilder().add("language", alt.get(i).getString("@language"))
						.add("name", alt.get(i).getString("@value")).build();
				alternatives.add(v);
			}
		}
		
		return Json
				.objectBuilder()
				.add("id", parseId(orig.getString("@id")))
				// TODO
				.add("name", orig.getString("name"))
				.add("alternativeNames", alternatives)
				.add("location",
						Json.objectBuilder()
								.add("Longitude",
										orig.get("longitude").asDouble())
								.add("Latitude",
										orig.get("latitude").asDouble()))
				.build();
		
	}

	private String parseId(String idUri) {
		return ID_PREFIX + idUri.substring(idUri.lastIndexOf("/") + 1);
	}
	
	private String extractId(String id) {
		id = id.trim();
		if (id.startsWith(ID_PREFIX)) {
			return id.replace(ID_PREFIX, "");
		}
		return id;
	}

	private String extractNameQuery(String name) {
		name = name.trim().toLowerCase();
		if (name.contains("[")) {
			return name.substring(0, name.indexOf('[')).trim();
		}
		return name;
	}

}
