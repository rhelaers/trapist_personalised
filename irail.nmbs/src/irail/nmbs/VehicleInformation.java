package irail.nmbs;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.ApplySchedule;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;

import tools.irail.IRail;

@Validated(input=false,output=false)
@Segment(id = "be.irail.nmbs.vehicle-info", tags = {
		"nmbs", "irail" })
public class VehicleInformation implements FunctionalSegment{

	@Service
	private Client httpClient;

	@InputDoc(schemaRef="irail.nmbs.model.VehicleInformation_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/irail/nmbs/vehicle")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
//		IRailNMBS iRail=new IRailNMBS();
		IRail iRail = new IRail();
		String id = input[0].getString("id");
		boolean fast = input[0].has("fast") ? input[0].getBool("fast") : true;
		String lang = input[0].has("lang") ? input[0].getString("lang")
				: "EN";
		JsonValue json = iRail.getVehicleInformation(httpClient,id, fast, lang);
		return json;
		
	}


	
}
