@org.osgi.annotation.versioning.Version("0.1.0")
@org.ibcn.limeds.annotations.IncludeTypes({
	"StationsProducer_input.json", 
	"StationsProducer_output.json", 
	"RouteFromTo_input.json", 
	"NearbyStationProducer_input.json", 
	"VehicleInformation_input.json",
	"ConnectionsProducer_input.json"})
package irail.nmbs.model;

