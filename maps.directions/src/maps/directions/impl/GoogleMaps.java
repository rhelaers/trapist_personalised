package maps.directions.impl;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonObjectBuilder;
import org.ibcn.limeds.json.JsonValue;

@Validated(input=false,output=false)
@Segment(id = "GoogleMapsDirection", tags = "producer")
public class GoogleMaps implements FunctionalSegment {

	private static final String API_KEY = "AIzaSyDG7za0pyPtp3AUCCWf4PhlFEETG6cmi8g";
	private static final String API_ENDPOINT = "https://maps.googleapis.com/maps/api/directions/json?";

	@Service
	private Client httpClient;
	
	@InputDoc(schemaRef="maps.directions.model.Direction_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/tools/maps/directions")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		JsonObject route = (JsonObject) input[0];String origin=route.getString("origin").replace(" ", "+");
		String destination=route.getString("destination").replace(" ", "+");
		String preferences=route.get("preferences").asString();	
		
		String url=API_ENDPOINT.concat("&origin="+origin)
				.concat("&destination="+destination)
				.concat("&mode=transit")
				.concat("&alternatives=false")
				//.concat("&language=nl")
				.concat(preferences)
				.concat("&key="+API_KEY);

		ClientResponse<JsonValue> response = httpClient.target(url)
				.get().returnObject(Json.getDeserializer());
		
		if (response.status() == 200) {
			JsonArray result=new JsonArray();
			JsonArray routes=response.getBody().get("routes").asArray();
			for(int i=0; i<routes.size(); i++){
				JsonArray legs=routes.get(i).get("legs").asArray();
				JsonArray newRoute=new JsonArray();
				for(int j=0; j<legs.size(); j++){
					JsonValue leg=legs.get(j);					
					
					Long date =leg.has("departure_time")? leg.get("departure_time").getLong("value"): LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(1));
							
					String startTijd=date.toString();
					
					int duur=leg.get("duration").get("value").asInt()/60;
					String duurLeesbaar=duur/60 +"u. " +duur%60+" min";
					
					
					JsonArray steps=leg.get("steps").asArray();
					JsonObject newSteps=new JsonObject();
					for(int k=0; k<steps.size(); k++){
						JsonValue step=steps.get(k);
												
						if(step.get("travel_mode").asString().equals("TRANSIT")){
							newRoute.add(Json.objectBuilder()
									.add("start", step.get("transit_details").get("departure_stop"))
									.add("stop",step.get("transit_details").get("arrival_stop"))															
									.add("afstand", step.get("distance").get("value"))
									.add("duur", step.get("duration").get("value"))
									.add("reistype", step.get("travel_mode"))
									.add("transit_details", Json.objectBuilder()
											.add("type", step.get("transit_details").get("line").get("vehicle").get("name"))
											.add("agency", step.get("transit_details").get("line").get("agencies").get(0).get("name"))
											.add("short_name", step.get("transit_details").get("line").getString("short_name"))
											.add("vertrekstation",(step.get("transit_details").get("departure_stop").getString("name")))
											.add("aankomststation", (step.get("transit_details").get("arrival_stop").getString("name")))
											.add("richting", step.get("transit_details").get("headsign")).build())
									.build());
						}
						else{
							newRoute.add(Json.objectBuilder()
									.add("start", step.get("start_location"))
									.add("stop",step.get("end_location"))															
									.add("afstand", step.get("distance").get("value"))
									.add("duur", step.get("duration").get("value"))
									.add("reistype", step.get("travel_mode"))
									.build());
						}
						
					}
					newSteps.put("departure_time", startTijd);
					newSteps.put("steps", newRoute);
					result.add(newSteps);
				}
				
			}
			
			return result;
			
		} else {
			throw new Exception("Invalid response!");
		}
	}

	private String removeStation(String string) {
		return string.replace("Station ", "");
	}

}
