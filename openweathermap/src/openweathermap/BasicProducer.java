package openweathermap;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

@Validated(input=false,output=false)
@Segment(id = "OpenWeatherMapAPI", tags = "input")
public class BasicProducer implements FunctionalSegment{

	private static final String ENDPOINT = "http://api.openweathermap.org/data/2.5/weather";
	private static final String API_KEY = "5ed560720f115ecd11bd70748cba4c94";
	
	@Service
	private Client httpClient;
	
	@InputDoc(schemaRef="openweathermap.model.Openweathermap_input_0.1.0")
	@HttpOperation(method = HttpMethod.POST, path = "/openweathermap/producer")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		JsonObject location = (JsonObject) input[0];
		ClientResponse<JsonObject> response = httpClient.target(ENDPOINT)
				.withQuery("lat", location.getString("latitude"))
				.withQuery("lon", location.getString("longitude"))
				.withQuery("units", "metric").withQuery("APPID", API_KEY).get().returnObject(Json.getDeserializer());
		JsonObject result = Json
				.objectBuilder()
				.add("temperature",
						response.getBody().get("main").get("temp"))
				.add("weather", response.getBody().get("weather").get(0).get("main"))
				.add("weatherId", response.getBody().get("weather").get(0).get("id"))
				.build();
		return result;
	}
	

}
