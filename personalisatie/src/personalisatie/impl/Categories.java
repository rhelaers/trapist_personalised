package personalisatie.impl;

import java.util.List;


public class Categories  {


    public static int getNumericValueForString(String name) {
        int value = 0;
        
        switch (name) {
            case "http://users.atlantis.ugent.be/svrstich/demoOntology#DepartureTimeSuitableConnection":
                value = 1;
                break;
            case "http://users.atlantis.ugent.be/svrstich/demoOntology#WalkingSuitableConnection":
                value = 2;
                break;
        }

        return value;
    }

}
