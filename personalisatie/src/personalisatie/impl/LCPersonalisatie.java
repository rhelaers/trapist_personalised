package personalisatie.impl;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientContext;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Link;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

@Segment(id = "LCPersonalisatieImpl")
public class LCPersonalisatie implements FunctionalSegment {
	
	@Link(target = "PersonalisatieImpl")
	private FunctionalSegment personalisatieImpl;
	
	@Service
	private Client httpClient;

	@HttpOperation(method = HttpMethod.GET, path = "/endpoint/LCpersonalisatie/{input}" )
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		// TODO Auto-generated method stub
		String res=null;
		if(input[1].has("request")){
			res = input[1].get("request").get("path").getString("input");
		}
						
		//return Json.objectBuilder().add("resultaat", res).build();
		
		
//		JsonValue metadata=Json.from("{\"hydra\":\"http://www.w3.org/ns/hydra/core#\", \"property\":{ \"@id\":\"hydra:property\", \"@type\":\"@vocab\" }, \"required\":\"hydra:required\", \"Collection\":\"hydra:Collection\", \"member\":{ \"@id\":\"hydra:member\", \"@type\":\"@id\" }, \"search\":\"hydra:search\", \"PagedCollection\":\"hydra:PagedCollection\", \"TemplatedLink\":\"hydra:TemplatedLink\", \"IriTemplate\":\"hydra:IriTemplate\", \"template\":\"hydra:template\", \"mapping\":\"hydra:mapping\", \"IriTemplateMapping\":\"hydra:IriTemplateMapping\", \"variable\":\"hydra:variable\", \"lc\":\"http://semweb.mmlab.be/ns/linkedconnections#\", \"gtfs\":\"http://vocab.gtfs.org/terms#\", \"Connection\":\"http://semweb.mmlab.be/ns/linkedconnections#Connection\", \"arrivalTime\":\"lc:arrivalTime\", \"departureTime\":\"lc:departureTime\", \"arrivalStop\":{ \"@type\":\"@id\", \"@id\":\"http://semweb.mmlab.be/ns/linkedconnections#arrivalStop\" }, \"departureStop\":{ \"@type\":\"@id\", \"@id\":\"http://semweb.mmlab.be/ns/linkedconnections#departureStop\" }}");
//				
//		return Json.objectBuilder()
//				.add("@context", metadata)
//				.add("@id", "http://localhost:3000/endpoint/LCpersonalisatie")
//				.add("@graph",personalisatieImpl.apply(input))
//				.build();
		
		return Json.from("{ \"@context\":{ \"hydra\":\"http://www.w3.org/ns/hydra/core#\", \"property\":{ \"@id\":\"hydra:property\", \"@type\":\"@vocab\" }, \"required\":\"hydra:required\", \"Collection\":\"hydra:Collection\", \"member\":{ \"@id\":\"hydra:member\", \"@type\":\"@id\" }, \"search\":\"hydra:search\", \"PagedCollection\":\"hydra:PagedCollection\", \"nextPage\":{ \"@id\":\"hydra:nextPage\", \"@type\":\"@id\" }, \"previousPage\":{ \"@id\":\"hydra:previousPage\", \"@type\":\"@id\" }, \"TemplatedLink\":\"hydra:TemplatedLink\", \"IriTemplate\":\"hydra:IriTemplate\", \"template\":\"hydra:template\", \"mapping\":\"hydra:mapping\", \"IriTemplateMapping\":\"hydra:IriTemplateMapping\", \"variable\":\"hydra:variable\", \"lc\":\"http://semweb.mmlab.be/ns/linkedconnections#\", \"gtfs\":\"http://vocab.gtfs.org/terms#\", \"Connection\":\"http://semweb.mmlab.be/ns/linkedconnections#Connection\", \"arrivalTime\":\"lc:arrivalTime\", \"departureTime\":\"lc:departureTime\", \"arrivalStop\":{ \"@type\":\"@id\", \"@id\":\"http://semweb.mmlab.be/ns/linkedconnections#arrivalStop\" }, \"departureStop\":{ \"@type\":\"@id\", \"@id\":\"http://semweb.mmlab.be/ns/linkedconnections#departureStop\" }, \"trip\":{ \"@type\":\"@id\", \"@id\":\"gtfs:trip\" } }, \"@id\":\"http://localhost:3000/endpoint/LCpersonalisatie/hallo\", \"@type\":\"PagedCollection\", \"nextPage\":\"http://localhost:3000/endpoint/LCpersonalisatie/hallo\", \"previousPage\":\"http://localhost:3000/endpoint/LCpersonalisatie/hallo\", \"search\":{ \"@type\":\"IriTemplate\", \"template\":\"http://localhost:3000/endpoint/LCpersonalisatie/{input}\", \"variableRepresentation\":\"BasicRepresentation\", \"mapping\":{ \"@type\":\"IriTemplateMapping\", \"variable\":\"input\", \"required\":true, \"property\":\"http://semweb.mmlab.be/ns/linkedconnections#departureTimeQuery\" } }, \"@graph\":[ { \"@type\":\"Connection\", \"arrivalTime\":\"2015-10-12T05:38:00.000Z\", \"arrivalStop\":\"2\", \"departureTime\":\"2015-10-12T05:30:00.000Z\", \"departureStop\":\"1\", \"@id\":\"5609c2f663f63e82582fd352\" }, { \"@type\":\"Connection\", \"arrivalTime\":\"2015-10-12T05:42:00.000Z\", \"arrivalStop\":\"5\", \"departureTime\":\"2015-10-12T05:39:00.000Z\", \"departureStop\":\"2\", \"@id\":\"5609c2f663f63e82582fd353\" }, { \"@type\":\"Connection\", \"arrivalTime\":\"2015-10-12T05:50:00.000Z\", \"arrivalStop\":\"3\", \"departureTime\":\"2015-10-12T05:40:00.000Z\", \"departureStop\":\"2\", \"@id\":\"5609c2f663f63e82582fd359\" } ] }");
	
	}

}




