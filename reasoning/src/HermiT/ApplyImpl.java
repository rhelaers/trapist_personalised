package HermiT;

import java.io.FileOutputStream;
import java.io.InputStream;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.ApplySchedule;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import de.derivo.sparqldlapi.Query;
import de.derivo.sparqldlapi.QueryEngine;
import de.derivo.sparqldlapi.QueryResult;

@Validated(input=false,output=false)
@Segment(id="HermiT")
public class ApplyImpl implements FunctionalSegment {
	
	private ReasonerImpl r;
	//private String url="demoOntologyZonderRules.owl";
	private String url="TestInheritance.owl";
	

	@Override
	public void started() throws Exception {
		// TODO Auto-generated method stub
		FunctionalSegment.super.started();
		r=new ReasonerImpl();
	}
		

	@InputDoc(schemaRef="reasoning.model.ApplyImpl_input_0.1.0")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
				
		String request = input[0].getString("type");
		switch (request) {
		case "add":
			String jsonld = input[0].get("body").toString();
			r.addKnowledge(jsonld);
			break;
		case "get":
			String query = input[0].getString("query")
					.replace("\\", "");
			return Json.from(r.query(
					query));
		case "flush":
			r.destroyInstance();
			r.createInstance(ApplyImpl.class.getResourceAsStream(url));
			break;
		case "dump":
			r.dumpModel(new FileOutputStream(
					"Ontologie" + "-" + System.currentTimeMillis()
							+ ".owl"));
			
			break;
		}
	return null;
	}
	

}
