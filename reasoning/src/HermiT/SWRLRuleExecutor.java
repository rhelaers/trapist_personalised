package HermiT;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.SWRLArgument;
import org.semanticweb.owlapi.model.SWRLAtom;
import org.semanticweb.owlapi.model.SWRLPredicate;
import org.semanticweb.owlapi.model.SWRLRule;

import de.derivo.sparqldlapi.Query;
import de.derivo.sparqldlapi.QueryArgument;
import de.derivo.sparqldlapi.QueryBinding;
import de.derivo.sparqldlapi.QueryEngine;
import de.derivo.sparqldlapi.QueryResult;
import swrlBuiltIn.SWRLBuiltIn;
import swrlBuiltIn.swrlb_add;
import swrlBuiltIn.swrlb_greaterThan;

public class SWRLRuleExecutor {
	private List<String> SWRLVariables;
	private List<SWRLBuiltIn> builtIn;
	
	private OWLOntology o;
	private OWLOntology superOntolgy;
	private OWLOntologyManager manager;


	public SWRLRuleExecutor(OWLOntology superOntolgy, OWLOntology o,OWLOntologyManager manager) {
		this.superOntolgy=superOntolgy;
		this.o=o;
		this.manager=manager;
	}
	
	public OWLOntology executeSWRLRules() throws Exception{
		Set<SWRLRule> rules = superOntolgy.getAxioms(AxiomType.SWRL_RULE);
		for(SWRLRule rule: rules){
			SWRLVariables=new ArrayList<>();
			builtIn=new ArrayList<>();
			StringBuilder total=new StringBuilder();
			StringBuilder builder=new StringBuilder();
			total.append(getPrefixes());
			Set<SWRLAtom> body= rule.getBody();
			for(SWRLAtom atom: body){
				if(!atom.toString().startsWith("BuiltInAtom")){
					String q=makeQueryString(atom);				
					builder.append(q);					
				}
				else {
					createSWRLBuiltIn(atom);
				}				
			}	
					
		String var="";
		for(String s:SWRLVariables){
			var+="?"+s+" ";
		}
		total.append(" SELECT ").append(var).append("WHERE { ");
		String q=builder.toString();
		if(q.endsWith(",")){
			q=q.substring(0, q.length()-1);
		}
		total.append(q).append(" }");
		Reasoner hermit=new Reasoner(o);
		QueryEngine engine = QueryEngine.create(manager, hermit);
		Query query = Query.create(total.toString());
		QueryResult result = engine.execute(query);
		Iterator<QueryBinding> it=result.iterator();
		while(it.hasNext()){
			QueryBinding b=it.next();
			HashMap<String,String> variables=new HashMap<>();
			for(QueryArgument arg:b.getBoundArgs()){
				variables.put(arg.toString(), b.get(arg).toString().replaceAll("\"", ""));
			}
			if(checkBuiltIn(variables)){
				Set<SWRLAtom> head= rule.getHead();
				for(SWRLAtom atom:head){
					ExecuteSWRLHead(atom,variables);
				}
				
			}
			
		}

		}
		
		return o;
		
	}
	
	private void ExecuteSWRLHead(SWRLAtom atom, HashMap<String, String> variables) throws Exception, FileNotFoundException {
		OWLDataFactory factory=manager.getOWLDataFactory();
		if(atom.toString().contains("ClassAtom")){
			String res=get2PartAtom(atom.toString());
			String varName=res.substring(0, res.indexOf(","));
			String className=res.substring(res.indexOf(",")+2,res.length()-1);
			String varValue="";
			for(String key:variables.keySet()){
				if(key.equals(varName)){
					varValue=variables.get(key);
				}
			}
						
			OWLIndividual ind=factory.getOWLNamedIndividual(IRI.create(varValue));
			OWLClass cl=factory.getOWLClass(IRI.create(className));
			
			OWLClassAssertionAxiom axiom=factory.getOWLClassAssertionAxiom(cl, ind);
			AddAxiom addAxiom=new AddAxiom(o, axiom);
			manager.applyChange(addAxiom);
			}		
	}
	
	private boolean checkBuiltIn(HashMap<String, String> variables) {
		for(SWRLBuiltIn swrlb: builtIn){
			switch(swrlb.getName()){
			case "add":
				swrlb_add Add=(swrlb_add)swrlb;
				Add.add(variables);
				break;
			case "greaterThan":
				swrlb_greaterThan Gt=(swrlb_greaterThan)swrlb;
				Boolean res=Gt.greaterThan(variables);
				if(!res) return false;
				break;
			}
		}
		return true;
	}

	public String getPrefixes(){
		return "PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+ "PREFIX  owl: <http://www.w3.org/2002/07/owl#> "
				+ "PREFIX  vocab: <http://vocab.org/transit/terms#> "
				+ "PREFIX  demoTLV: <http://users.atlantis.ugent.be/svrstich/demoOntology#> "
				;
	}
	
	private String makeQueryString(SWRLAtom atom) {
		StringBuilder b=new StringBuilder();
		if(atom.toString().contains("ClassAtom")){
			b.append("Type(");
			b.append(get2PartAtom(atom.toString()));
			b.append("),");
			
		}else if(atom.toString().contains("DataPropertyAtom")){
			b.append("Annotation(");
			b.append(get3PartAtom(atom.toString()));
			b.append("),");
			
		}else if(atom.toString().contains("ObjectPropertyAtom")){
			b.append("Annotation(");
			b.append(get3PartAtom(atom.toString()));
			b.append("),");
		}
				
		return b.toString();
	}
	
	private void createSWRLBuiltIn(SWRLAtom atom) {

		List<SWRLArgument> l=(List<SWRLArgument>) atom.getAllArguments();
		switch(getBuiltInType(atom.getPredicate().toString())){
		case "add":
			SWRLBuiltIn add=new swrlb_add(getBuiltInValue(l.get(0)),getBuiltInValue(l.get(1)),getBuiltInValue(l.get(2)));
			builtIn.add(add);
			break;
			
		case "greaterThan":
			SWRLBuiltIn gt=new swrlb_greaterThan(getBuiltInValue(l.get(0)),getBuiltInValue(l.get(1)));
			builtIn.add(gt);
			break;
		}
	}

	private String getBuiltInValue(SWRLArgument swrlArgument) {
		String arg=swrlArgument.toString();
		if(arg.startsWith("Variable")){
			Pattern p=Pattern.compile(".*#(.*)>.*");
			Matcher matcher = p.matcher(arg);
			if (matcher.find())
			{
				return "?"+matcher.group(1);
			}
		}
		else if(arg.contains("^^xsd:integer")){
			Pattern p=Pattern.compile("\"(.*)\"");
			Matcher matcher = p.matcher(arg);
			if (matcher.find())
			{
				return matcher.group(1);
			}
		}
		return arg;
	}

	private String getBuiltInType(String arg) {
		Pattern p=Pattern.compile(".*#(.*)");
		Matcher matcher = p.matcher(arg);
		if (matcher.find())
		{
			return matcher.group(1);
		}
		return arg;
	}

	private String get2PartAtom(String arg) {
		StringBuilder b=new StringBuilder();
		Pattern p=Pattern.compile(".*(<.*>).*(<.*>).*");
		Matcher matcher = p.matcher(arg);
		if (matcher.find())
		{
			b.append(GetVariable(1,arg,matcher.group(2))).append(",").append(GetVariable(2,arg,matcher.group(1)));
		    return b.toString();
		}
		return arg;
	}
	
	private String get3PartAtom(String arg) {
		StringBuilder b=new StringBuilder();
		Pattern p=Pattern.compile(".*(<.*>).*(<.*>).*(<.*>).*");
		Matcher matcher = p.matcher(arg);
		if (matcher.find())
		{
			b.append(GetVariable(1,arg,matcher.group(2))).append(",").append(GetVariable(2,arg,matcher.group(1))).append(",").append(GetVariable(3,arg,matcher.group(3)));
		    return b.toString();
		}
		return arg;
	}
	
	private String GetVariable(int plaats,String arg, String group) {
		if(arg.contains("Variable("+group+")")){
			Pattern p=Pattern.compile(".*#(.*)>.*");
			Matcher matcher = p.matcher(group);
			if (matcher.find())
			{
				if(!SWRLVariables.contains(matcher.group(1))){
					SWRLVariables.add(matcher.group(1));
				}
				
			    return "?"+matcher.group(1);
			}
		}else if(plaats==3){ //Wanneer objects op plaats 3 staat moet het ge�nterpreteerd worden als string en niet als object (wnr in annotatie gebruikt)
			return group.replace('<','"').replace('>', '"');
		}
		return group;
	}

}
