package swrlBuiltIn;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.mockito.ReturnValues;

import de.derivo.sparqldlapi.QueryArgument;
import de.derivo.sparqldlapi.QueryBinding;

public class swrlb_add implements SWRLBuiltIn{
	
	private String returnValue;
	private String value1;
	private String value2;
	
	public swrlb_add(String r,String v1,String v2){
		returnValue=r;
		value1=v1;
		value2=v2;
	}
	
		
	@Override
	public String getName() {
		return "add";
	}

	public void add(HashMap<String, String> variables) {
		long x1=-1;
		long x2=-1;
		boolean x1bool=false;
		boolean x2bool=false;
		for(String key:variables.keySet()){
			//System.out.println("key="+key+" val="+variables.get(key)+" value1="+value1+" value2="+value2);
			if(key.equals(value1)){
				//System.out.println(1);
				x1=Long.parseLong(variables.get(key));
				x1bool=true;
			}else if(key.equals(value2)){
				//System.out.println(2);
				x2=Long.parseLong(variables.get(key));
				x2bool=true;
			}
			
		}
		if(!x1bool){
			//System.out.println(3);
			x1=Long.parseLong(value1);
			
		}
		if(!x2bool){
			//System.out.println(4);
			x2=Long.parseLong(value2);
			
		}
				
		long out=x1+x2;
		variables.put(returnValue, ""+out);
			
			
	}
}
