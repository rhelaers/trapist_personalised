package swrlBuiltIn;

import java.util.HashMap;

import de.derivo.sparqldlapi.QueryBinding;

public class swrlb_greaterThan implements SWRLBuiltIn {
	
	private String value1;
	private String value2;
	
	public swrlb_greaterThan(String v1,String v2){
		value1=v1;
		value2=v2;
	}

	public boolean greaterThan(HashMap<String, String> variables){
		long x1=-1;
		long x2=-1;
		boolean x1bool=false;
		boolean x2bool=false;
		for(String key:variables.keySet()){
			if(key.equals(value1)){
				x1=Long.parseLong(variables.get(key));
				x1bool=true;
			}else if(key.equals(value2)){
				x2=Long.parseLong(variables.get(key));
				x2bool=true;
			}			
		}
		if(!x1bool){
			x1=Long.parseLong(value1);			
		}
		if(!x2bool){
			x2=Long.parseLong(value2);			
		}			
		return x1>x2;
	}

	@Override
	public String getName() {
		return "greaterThan";
	}
}
