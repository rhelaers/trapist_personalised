package tools.irail;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;

public class IRail {
		public JsonValue getLiveboard(Client httpClient, JsonValue stop, LocalDateTime date)
			throws UnsupportedOperationException, Exception {
		String id, url;
		id="/?station=".concat(stop.getString("id"));
		String dag=date.format(DateTimeFormatter.ofPattern("ddMMyy"));
		String tijd=date.format(DateTimeFormatter.ofPattern("HHmm"));
		url = "https://api.irail.be/liveboard".concat(id).concat("&date="+dag).concat("&time="+tijd).concat("&fast=true&format=json");
		return httpClient.target(url)
					.withHeader("accept", "application/json").get()
					.returnObject(Json.getDeserializer()).getBody();
	}

	public JsonArray getStationsNMBS(Client httpClient, String query) throws Exception {
		String ENDPOINT_URL="https://irail.be/stations/NMBS";
		if(query != null && !query.isEmpty()){
			ClientResponse<JsonValue> response = httpClient.target(ENDPOINT_URL)
					.withHeader("Accept", "application/json").withQuery("q", query)
					.get().returnObject(Json.getDeserializer());
			return (JsonArray) response.getBody().get("@graph");
		}
		else{
			ClientResponse<JsonValue> response = httpClient.target(ENDPOINT_URL)
					.withHeader("Accept", "application/json")
					.get().returnObject(Json.getDeserializer());
			return (JsonArray) response.getBody().get("@graph");
		}
		
	}

	public JsonValue getVehicleInformation(Client httpClient, String id, boolean fast, String lang)
			throws Exception {			
			String url = "https://api.irail.be/vehicle/?id=";
			url = url.concat(id).concat("&fast=")
					.concat(Boolean.toString(fast)).concat("&lang=")
					.concat(lang).concat("&format=json");
			return httpClient.target(url).get().returnObject(Json.getDeserializer()).getBody();
		
		}
	
	public JsonValue getRoutesFromTo(Client httpClient, String query) throws Exception{
		String ENDPOINT_URL="https://api.irail.be/connections/?";
		String url=ENDPOINT_URL.concat(query);
		ClientResponse<JsonValue> response = httpClient.target(url)
				.withHeader("Accept", "application/json")
				.get().returnObject(Json.getDeserializer());
		return response.getBody();
	}
	
	public JsonValue getDepartures(Client httpClient, JsonValue stop, LocalDateTime date)
			throws Exception {
		String BASE="https://data.irail.be/DeLijn/Departures";
		String id = "/".concat(stop.getString("id").replaceAll(" ", "%20"));
		String DATE_FORMAT=date.format(DateTimeFormatter.ofPattern("/yyyy/MM/dd/HH/mm"));
		String url = BASE.concat(id).concat(DATE_FORMAT).concat(".json");
		return httpClient.target(url).get().returnObject(Json.getDeserializer()).getBody();
				
	}
	
	public JsonValue getDeparturesMIVBSTIB(Client httpClient, JsonValue stop, LocalDateTime date)
			throws Exception {
		String BASE="https://data.irail.be/MIVBSTIB/Departures";
		String id = "/".concat(stop.getString("id").replaceAll(" ", "%20"));
		String DATE_FORMAT=date.format(DateTimeFormatter.ofPattern("/yyyy/MM/dd/HH/mm"));
		String url = BASE.concat(id).concat(DATE_FORMAT).concat(".json");
		return httpClient.target(url).get().returnObject(Json.getDeserializer()).getBody();
				
	}

	public JsonValue getStationsDeLijn(Client httpClient, String query) throws Exception {
		String url="https://data.irail.be/DeLijn/Stations.json";
		if(query != null && !query.isEmpty()){
			return httpClient.target(url).withQuery("q", query).get().returnObject(Json.getDeserializer()).getBody();
		}
		return httpClient.target(url).get().returnObject(Json.getDeserializer()).getBody().get("Stations");
	}
	
}
